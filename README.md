# AP Computer Science Backup Repository
## labs
### Jeroo
A game to learn the basics of java
1. Jeroo Lab 1-2 ~/jeroo/Lab1-2
3. Jeroo Lab 2-4 ~/jeroo/Lab2-4
4. Jeroo Lab 3-4 ~/jeroo/Lab3-2
5. Jeroo Lab 4-2 ~/jeroo/Lab4-2
7. Jeroo Lab 5-1 ~/jeroo/Lab5-1
8. Jeroo Quiz ~/jeroo/jerooQuiz
### Java
#### Labs
1. Lab 0a2 Ascii Box java/labs/lab0a2/
2. Lab 0b Variables java/labs/Lab0b/
3. Lab 0c Input Basics java/labs/Lab0c/
4. Lab 1a Stars and Stripes java/labs/Lab1a/
5. Lab 1b Smiley Face java/labs/Lab1b/
6. Lab 2-5 Circle java/labs/Lab-2-5/
7. Lab 3-4 Distance java/labs/Lab3-4/
8. Lab 4-5 Strings java/labs/Lab4-5/
9. Lab 8 Mult Table java/labs/Lab8/
10. Lab 9-2 Digit Summer java/labs/Lab9-2/
11. Lab 10 Rock Paper Scissers java/labs/Lab10/
12. Lab 11 Trinagle Letters java/labs/Lab11/
13. Lab 12-3 Average Line java/labs/Lab12-3/
14. Lab 13-6 Primes java/labs/Lab13-6/
16. Lab 14 Find Smallest java/labs/Lab14/
17. Lab 16 list going down java/labs/Lab16/
#### Projects
1. Piggy Bank ~/java/projects/piggyBank/
2. Rental Car ~/java/projects/carRental/
3. Palindrome ~/java/projects/Palindrome/
4. Statistics ~/java/projects/Statistics/
5. CasinoGame ~/java/projects/casinoGame/
6. Polygons ~/java/projects/polygon
7. farmAnimal
8. runtimeAnalysis
9. Words
#### Test
1. test ~/java/test/
