//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import java.util.Scanner;

public class StringRunner
{
	public static void main ( String[] args )
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a word :: ");
		String word = keyboard.next();
		
		StringStuff stuff = new StringStuff(word);
		System.out.println("has first last letters :: " + stuff.getFirstLastLetters() + "\nhas middle letter :: " + stuff.getMiddleLetter() + "\nhas same first and last letter :: " + stuff.sameFirstLastLetters());
		keyboard.close();
	}
}
