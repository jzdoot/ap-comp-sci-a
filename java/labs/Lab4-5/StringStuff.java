//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

public class StringStuff
{
	private String word;

	public StringStuff( String w )
	{
		word=w;
	}

 	public String getFirstLastLetters()
 	{
		String wordEnd = word.substring(word.length()-1);
		String wordStart = word.substring(0,(word.length()-(word.length()-1)));
 		return wordStart + wordEnd;
	}
	
 	public String getMiddleLetter()
 	{
 		return word.substring((word.length()/2),((word.length()/2)+1));
	}	
	
 	public boolean sameFirstLastLetters()
 	{
		String wordEnd = word.substring(word.length()-1);
		String wordStart = word.substring(0,(word.length()-(word.length()-1)));
 		return wordEnd==wordStart;
	}	

 	public String toString()
 	{
 		return "" + word;
	}
}
