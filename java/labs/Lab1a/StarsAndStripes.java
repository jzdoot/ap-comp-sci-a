// A+ Computer Science
//www.apluscompsci.com

//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*;

public class StarsAndStripes
{
   public StarsAndStripes()
   {
      out.println("StarsAndStripes");
      printTwoBlankLines();
	  printASmallBox();
      printTwoBlankLines();
	  printABigBox();
   }

   public void printTwentyStars()
   {
   }

   public void printTwentyDashes()
   {
   }

   public void printTwoBlankLines()
   {
	   out.println();
	   out.println();
   }
   
   public void printASmallBox()
   {	
	   out.println("--------------------\nxxxxxxxxxxxxxxxxxxxx\n--------------------\nxxxxxxxxxxxxxxxxxxxx\n--------------------\nxxxxxxxxxxxxxxxxxxxx\n--------------------");
   }
 
   public void printABigBox()
   { 	
	   printASmallBox();
	   printASmallBox();
   }   
}