//(c) A+ Computer Science
//www.apluscompsci.com
//Name - Jacob Samurin

import java.util.*;
import java.io.*; 

public class WordSearch
{
    private String[][] m;

    public WordSearch( int size, String str )
    {
		int count = 0;
		m = new String[size][size];
		for(int row = 0; row < size; row++)
		{
			for(int col = 0; col < size; col++)
			{
				m[row][col] = str.substring(count,count+1);
				count++;
			}
		}
    }

    public boolean isFound( String word )
    {
      boolean right = false, left = false, up = false, down = false, out = false;
      
      for(int i = 0; i < m.length; i++){
        for(int k = 0; k < m[i].length; k++){
          if(m[i][k].equals(word.substring(0,1))){
            right = checkRight(word.substring(1), i, k);
            left = checkLeft(word.substring(1), i, k);
            up = checkUp(word.substring(1), i, k);
            down = checkDown(word.substring(1), i, k);
            if(right || left || up || down){
				out=true;
            }
          }
        }
      }
	  return out;
    }

	public boolean checkRight(String w, int r, int c)
   {
	   boolean out = false;
	   if(c+1 > 7)
		   return out;
	   if(w.length() == 1 && m[r][c+1].equals(w.substring(0,1))){
		   out=true;
		   return true;
	   }
	   else if(w.length() == 1){
		   out = false;
		   return false;
	   }
	   if(!out&&m[r][c+1].equals(w.substring(0,1)))
		   return checkRight(w.substring(1), r, c+1);
	   return false;
   }

	public boolean checkLeft(String w, int r, int c)
	{
		if(c-1 < 0)
			return false;
		if(w.length() == 1 && m[r][c-1].equals(w.substring(0,1)))
			return true;
		else if(w.length() == 1)
			return false;
		if(m[r][c-1].equals(w.substring(0,1)))
			return checkLeft(w.substring(1), r, c-1);
		return false;
	}

	public boolean checkUp(String w, int r, int c)
	{
	   boolean out = false;
	   if(r-1<0)
		   return out;
	   if(w.length() == 1 && m[r-1][c].equals(w.substring(0,1))){
		   out=true;
		   return true;
	   }
	   else if(w.length() == 1){
		   out = false;
		   return false;
	   }
	   if(!out&&m[r-1][c].equals(w.substring(0,1)))
		   return checkUp(w.substring(1), r-1, c);
	   return false;
	}

	public boolean checkDown(String w, int r, int c)
   {
	   boolean out = false;
	   if(r+1 > 7)
		   return out;
	   if(w.length() == 1 && m[r+1][c].equals(w.substring(0,1))){
		   out=true;
		   return true;
	   }
	   else if(w.length() == 1){
		   out = false;
		   return false;
	   }
	   if(!out&&m[r+1][c].equals(w.substring(0,1)))
		   return checkRight(w.substring(1), r+1, c);
	   return false;
   }

	// public boolean checkDiagUpRight(String w, int r, int c)
	// {
	// 	return false;
	// }

	// public boolean checkDiagUpLeft(String w, int r, int c)
	// {
	// 	return false;
	// }

	// public boolean checkDiagDownLeft(String w, int r, int c)
   // {
	// 	return false;
	// }

	// public boolean checkDiagDownRight(String w, int r, int c)
	// {
	// 	return false;
	// }

    public String toString()
    {
		// make a toString that returns the matrix "m"
		String output = "";
		for(int row = 0; row < m.length; row++)
		{
			for(int col = 0; col < m[row].length; col++)
			{
				output += m[row][col] + " ";
			}
			output += "\n";
		}
		return output;
	}
}
