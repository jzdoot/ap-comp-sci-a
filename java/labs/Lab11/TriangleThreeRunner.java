//(c) A+ Computer Science
//www.apluscompsci.com
//Name - Jacob Samurin

import static java.lang.System.*;

public class TriangleThreeRunner
{
   public static void main( String args[] )
   {
	   TriangleThree tri = new TriangleThree(3,"A");
	   out.println(tri.toString());
	   tri.setTriangle("X",7);
	   out.println(tri.toString());
	   tri.setTriangle("R", 1);
	   out.println(tri.toString());
	   tri.setTriangle("T", 5);
	   out.println(tri.toString());
	   tri.setTriangle("W", 4);
	   out.println(tri.toString());
   }
}
