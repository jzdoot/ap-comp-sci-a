//(c) A+ Computer Science
//www.apluscompsci.com
//Name - Jacob Samurin

import static java.lang.System.*;

public class TriangleThree
{
   private int size;
   private String letter;

	public TriangleThree()
	{
		setTriangle("a", 1);
	}

	public TriangleThree(int count, String let)
	{
		setTriangle(let, count);
	}

	public void setTriangle( String let, int sz )
	{
		letter=let;
		size=sz;
	}

	public String getLetter() 
	{
		return "#";
	}

	public String toString()
	{
		String output = "";
		for(int i = 0; i <= size; i++) {
			for(int j = size; j>0;j--){
				if(i<j)
					output+= " ";
				else
					output += letter;
			}
			output += "\n";
		}
		return output + "\n";
	}
}
