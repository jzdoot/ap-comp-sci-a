//(c) A+ Computer Science
//www.apluscompsci.com
//Name -
//Date -

public class RaySmallest
{
	public static int go(int[] ray)
	{
		int smallest = ray[0];
		for(int inc  = 0; inc<ray.length; inc++){
			if(ray[inc]<smallest)
				smallest = ray[inc];
			// else if(ray[inc] >= ray[inc-1])
			// 	smallest = ray[inc-1];
		}
		return smallest;
	}
}
