//(c) A+ Computer Science
// www.apluscompsci.com
//Name -  

import java.util.Scanner;
import static java.lang.System.*;

public class RPSRunner
{
	public static void main(String args[])
	{
		Scanner k = new Scanner(System.in);
		char response;
		
		//add in a do while loop after you get the basics up and running
		
			String player = "";
		
				RockPaperScissors game = new RockPaperScissors();		
			do {
				
				// getting the selection
				out.print("Rock-Papaer-Scissors [R,P,S] :: ");
				player = k.next();
				game.setPlayers(player);
				out.println(game.determineWinner());
				out.print("Do you want to contune [y/n]");
				String str;
				str = k.next();
				response=str.charAt(0);
			} while (response == 'y' || response == 'Y');
			k.close();
	}
}
