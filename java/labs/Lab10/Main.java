// Let's do an important experiment! Please try coding this project entirely using your iPad instead of your computer. 

// Note in comments your successes and failures. If it is impossible - switch but state that here.

// Name - Jacob Samurin

/*
Comments on device used and success
1. Can't read instructions while in replit
2. I can't think in computer science terms while useing an iPad
after that I switch to my laptop
*/

class Main {
  public static void main(String[] args) {
    RPSRunner.main(args);
  }
}
