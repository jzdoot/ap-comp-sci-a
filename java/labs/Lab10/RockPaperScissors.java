//(c) A+ Computer Science
// www.apluscompsci.com
//Name -  

// import java.util.Scanner;
import static java.lang.System.*;
import java.lang.Math;

public class RockPaperScissors
{
	private String playChoice;
	private String compChoice;

	public RockPaperScissors()
	{
		playChoice = "S";
	}

	public RockPaperScissors(String player)
	{
		playChoice = player;
	}

	public void setPlayers(String player)
	{
		playChoice = player;
	}

	public String determineWinner()
	{
		String winner="";
		compChoice = (int)(Math.random() * 3) + 1 + "";
		switch (playChoice) {
			case "R":
				if(compChoice.equals("1"))
					winner = "Tie";
				else if(compChoice.equals("2"))
					winner = "Computer";
				else if(compChoice.equals("3"))
					winner = "Player";
				break;
			case "P":
				if(compChoice.equals("1"))
					winner = "Player";
				else if(compChoice.equals("2"))
					winner = "Tie";
				else if(compChoice.equals("3"))
					winner = "Computer";
				break;
			case "S":
				if(compChoice.equals("1"))
					winner = "Computer";
				else if(compChoice.equals("2"))
					winner = "Player";
				else if(compChoice.equals("3"))
					winner = "Tie";
		}
		// out.println(compChoice);
		return winner;
		// return compChoice;
	}

	public String toString()
	{
		String output="";
		return output;
	}
}
