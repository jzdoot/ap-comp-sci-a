//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import java.util.Arrays;
import java.util.Scanner;
import static java.lang.System.*;
import static java.lang.Math.*;
import static java.util.Arrays.*;
import static java.util.Collections.*;

public class Monsters
{
 private  int size;
 private Monster[] myMonsters;

 public Monsters(int s)
 {
	 size = s;
	 myMonsters = new Monster[size];
 }

 public void add(int spot, Monster m)
 {
	 myMonsters[spot] = m;
  //put m in the Monster array at [spot]
 }

 public Monster getLargest( )
 {
	Arrays.sort(myMonsters);
	return myMonsters[myMonsters.length-1];
 }

 public Monster getSmallest( )
 {
	Arrays.sort(myMonsters);
	return myMonsters[0];
 }

 public String toString()
 {
	 String output ="[";
	 for (Monster item : myMonsters) {
		 output += " " + item.toString() + ",";
	 }
	 output += "]";
  return output;
 }
 public String sorted(){
	 Arrays.sort(myMonsters);
	 String output ="[";
	 for (Monster item : myMonsters) {
		 output += " " + item.toString() + ",";
	 }
	 output += "]";
  return output;
 }
}
