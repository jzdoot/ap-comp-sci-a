//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import java.util.Arrays;
import java.util.Scanner;
import static java.lang.System.*;
import static java.util.Arrays.*; 

public class MonsterLabRunner
{
   public static void main( String args[] )
   {
  Scanner k = new Scanner(in);
  
  out.print("How many monsters are in the herd? :: ");
  int size = k.nextInt();
  Monsters herd = new Monsters(size);

  for(int i=0; i<size;i++)
  {
   //read in monster values 
	  String tempS = "";
	  int tempI;
   //ask for name
	  out.print("Enter the name :: ");
	  tempS = k.next();
   //ask for size
	  out.print("Enter the size :: ");
	  tempI = k.nextInt();
   //instantiate a new Monster() and add it to the herd   
	  Monster m = new Monster(tempS, tempI);
	  herd.add(i, m);
  }  
  k.close();
  
  System.out.println("HERD :: "+herd+"\n");
  //print out the other stats
  out.println("Smallest :: " + herd.getSmallest());
  out.println("Largest :: " + herd.getLargest());
  out.println("Sorted :: " + herd.sorted());
   
 }  
}
