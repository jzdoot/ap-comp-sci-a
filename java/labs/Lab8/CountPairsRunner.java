//(c) A+ Computer Science
//www.apluscompsci.com
//Name

import static java.lang.System.*; 

public class CountPairsRunner
{
	public static void main( String[] args )
	{
		System.out.println( CountPairs.pairCounter("test_cases") );
		//add in all of the provided test cases from the lab handout	
		CountPairs pair = new CountPairs();
		out.println(pair.pairCounter("ddogccatppig"));
		out.println(pair.pairCounter("dogcatpig"));
		out.println(pair.pairCounter("xxyyzz"));
		out.println(pair.pairCounter("a"));
		out.println(pair.pairCounter("abc"));
		out.println(pair.pairCounter("aabb"));
		out.println(pair.pairCounter("dogcatpigaabbcc"));
		out.println(pair.pairCounter("aabbccdogcatpig"));
		out.println(pair.pairCounter("dogabbcccatpig"));
		out.println(pair.pairCounter("aaaa"));
		out.println(pair.pairCounter("AAAAAAAAA"));
	}
}
