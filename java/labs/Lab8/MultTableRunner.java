//(c) A+ Computer Science
//www.apluscompsci.com
//Name

import static java.lang.System.*;

public class MultTableRunner
{
	public static void main ( String[] args )
	{
		MultTable man = new MultTable(5,5);
		out.println("multipcation table for 5");
		man.printTable();
		man.setTable(3,7);
		out.println("\n\nmultipcation table for 3");
		man.printTable();
		man.setTable(1,6);
		out.println("\n\nmultipcation table for 1");
		man.printTable();
		man.setTable(9,9);
		out.println("\n\nmultipcation table for 9");
		man.printTable();
		man.setTable(7,8);
		out.println("\n\nmultipcation table for 7");
		man.printTable();
	}
}
