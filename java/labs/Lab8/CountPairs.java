//(c) A+ Computer Science
//www.apluscompsci.com
//Name

import static java.lang.System.*;

public class CountPairs
{
	public static int pairCounter( String str )
	{ 
		int count=0;
		for(int inc=0; inc<str.length()-1 ; inc++){
			if(str.substring(inc,inc+1) == str.substring(inc+1,inc+2))
				count++;
		}
		return count;
	}
}
