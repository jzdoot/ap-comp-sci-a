//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

// import static java.lang.System.*;

public class Monster implements Comparable
{
	private String name;
	private int howBig;	

	public Monster()
	{
		name = "Bob";
		howBig = 5;
	}

	public Monster(String n, int size)
	{
		name = n;
		howBig = size;
	}

	public String getName()
	{
		return name;
	}
	
	public int getHowBig()
	{
		return howBig;
	}
	
	public boolean isBigger(Monster other)
	{
		other = (Monster)other;
		return other.howBig > this.howBig;
	}
	
	public boolean isSmaller(Monster other)
	{
		//call isBigger() use !
		return !isBigger(other);
	}

	public boolean namesTheSame(Monster other)
	{
		other = (Monster)other;
		return other.name.equals(this.name);
		// return false;
	}
	
	public String toString()
	{
		return name + " " + howBig;
	}

	public int compareTo(Object obj) {
		Monster other = (Monster)obj;
		return this.howBig - other.howBig;
	}
}
