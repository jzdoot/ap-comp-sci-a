//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import java.util.Scanner;
import static java.lang.System.*;

public class MonsterRunner
{
	public static void main( String args[] )
	{
		String tempS= "";
		int tempI;
		Scanner k = new Scanner(System.in);
		
		//ask for name and size
		out.print("enter 1st monster's name : ");
		tempS = k.next();
		out.print("Enter 1st monster's size : ");
		tempI = k.nextInt();

		//instantiate monster one
		Monster one = new Monster(tempS, tempI);
		//ask for name and size
		out.print("enter 2st monster's name : ");
		tempS = k.next();
		out.print("Enter 2st monster's size : ");
		tempI = k.nextInt();
		
		//instantiate monster two
		Monster two = new Monster(tempS, tempI);
		//print mosters
		out.println("\nMonster 1 - " + one + "\n Monster 2 - " + two + "\n");
		//Compare
		if(one.isSmaller(two))
			out.println("Monster one is smaller then monster two");
		else
			out.println("Moster one is bigger then monster two");
		if(one.namesTheSame(two))
			out.println("Monster one does have the same name as Monster two");
		else
			out.println("Monster one does not have the same name as Monster two");
	}
}
