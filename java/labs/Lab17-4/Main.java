class Main {
  public static void main(String[] args) {
    TriangleWordRunner.main(args);

    // for this lab you can choose to input data either by:
    // 1. hard code all given test cases
    // 2. scanner input and make a loop to see if I want to play again
    // 3. gui input and make a loop to see if I want to play again

    // Your choice!
  }
}