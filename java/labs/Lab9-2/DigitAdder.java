// A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

// import static java.lang.System.*;

public class DigitAdder
{
	public static int go( int num )
	{
		int tempNum = num;
		int newNum = 0;
		while(tempNum > 0){
			int numRemainder = tempNum % 10;
			newNum += numRemainder;
			tempNum/=10;
		}
		return newNum;
	}
}
