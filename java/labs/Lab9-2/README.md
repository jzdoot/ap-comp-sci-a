# Summing Digits
## Lab Goal
This lab will focus on decision Making and iteration [ looping] while reviewing accessing numeric digits using mod and divide.
## Lab Description
Write a program that will sum all of a numbers digits. You must use % for this lab to access the right most digit of the number. you will use/to chop the right most digit.
## Sample Data
234
1000
111
9005
84645
123456789
55556468
8525455
8514548
111111
1212121212
222222
## Sample output
9
1
3
14
27
24
45
44
34
35
6
15
12
