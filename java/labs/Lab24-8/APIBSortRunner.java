//� A+ Computer Science  -  www.apluscompsci.com
//Name - Jacob Samurin
//Date - 03/30/2022
//Class - AP Computer Science A
//Lab  - APIB Sort 24-8

public class APIBSortRunner
{
 public static void main( String args[] )
 {
    APIBSort.insertionSort(new Comparable[]{9,5,3,2});

    APIBSort.selectionSort(new Comparable[]{9,5,3,2});

    APIBSort.mergeSort(new Comparable[]{9,5,3,2});

    APIBSort.bubbleSort(new Comparable[]{9,5,3,2});


    APIBSort.insertionSort(new Comparable[]{19,52,3,2,7,21});
// add other sorts

    APIBSort.insertionSort(new Comparable[]{68,66,11,2,42,31});
// add other sorts
 }
}
