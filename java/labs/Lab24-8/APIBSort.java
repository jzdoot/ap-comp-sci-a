// A+ Computer Science  -  www.apluscompsci.com
//Name - Jacob Samurin
//Date - 
//Class - 
//Lab  - 

import static java.lang.System.*;
import java.util.Arrays;  //use Arrays.toString() to help print out the array

public class APIBSort
{
 private static int passCount=1;

 public static void insertionSort(Comparable[] list)
 {
	 out.println("insertionSort\nPass 0" + Arrays.toString(list));
	 int pass=1;
	 for(int j=1;j<list.length;j++){
		 Comparable temp = list[j];
		 int possibleIndex = j;
		 while (possibleIndex>0 && temp.compareTo(list[possibleIndex -1]) < 0){
			 list[possibleIndex] = list[possibleIndex - 1];
			 possibleIndex--;
		 }
		 list[possibleIndex]=temp;
		 out.println("Pass "+pass+Arrays.toString(list));
		 pass++;
	 }
 }
public static void selectionSort(Comparable[] list)
 {
	 out.println("selectionSort\nPass 0"+Arrays.toString(list));
	 int pass=1;
	 for(int j=0;j<list.length -1; j++){
		 int minIndex = j;
		 for (int k=j +1 ; k < list.length; k++){
			 if (list[k].compareTo(list[minIndex])<0)
				 minIndex = k;
		 }
		 Comparable temp = list[j];
		 list[j] = list[minIndex];
		 list[minIndex] = temp;
		 out.println("Pass "+pass+Arrays.toString(list));
		 pass++;
	 }
 }
 public static void mergeSort(Comparable[] list)
 {
	 out.println("mergeSort\nPass 0"+Arrays.toString(list));
	 Comparable[] temp = new Comparable[list.length];
	 mergeSort(list, 0, list.length-1,temp);
 }

 public static void mergeSort( Comparable[] list, int front, int back,Comparable[] temp)  //O( Log N )
 {
	 //FIXME
	 if(front < back){
		 int middle = (front + back) / 2;
		 mergeSort(list, front, middle,temp);
		 mergeSort(list, middle+1, back,temp);
		 merge(list, front, middle, back,temp);
	 }
 }

 public static void merge(Comparable[] list, int front,int mid, int back,Comparable[] temp)  //O(N)
 {
	 //FIXME run :(
	 int i = front, j=mid+1, k=front;
	 while(i<=mid && j<=back){
		 if(list[i].compareTo(list[j])<0){
			 temp[k] = list[i];
			 i++;
		 }else{
			 temp[k] = list[j];
			 j++;
		 }
		 k++;
	 }
	 while(i<=mid){
		 temp[k] = list[i];
		 i++;
		 k++;
	 }
	 while(j<=back){
		 temp[k] = list[j];
		 j++;
		 k++;
	 }
	 for(k=front;k<= back;k++){
		 list[k]=temp[k];
	 }
	 out.println("Pass " +passCount + Arrays.toString(list));
	 passCount++;
 }

 public static void bubbleSort(Comparable[] list)
 {
	 out.println("bubbleSort\nPass 0"+Arrays.toString(list));
	 int pass=1;
	 for(int last = list.length-1;last>0;last--){
		 for(int current=0;current<last;current++){
			 if(list[current].compareTo(list[current+1])>0){
				 Comparable temp = list[current];
				 list[current]=list[current+1];
				 list[current+1]=temp;
			 }
		 }
	 out.println("Pass "+pass+Arrays.toString(list));
	 pass++;
	 }
 }
}
