//(c) A+ Computer Science
//www.apluscompsci.com

//Name -
//Date -
//Class -
//Lab  -

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Canvas;

public class SmileyFace extends Canvas
{
   public SmileyFace()    //constructor - sets up the class
   {
      setSize(800,600);
      setBackground(Color.BLACK);
      setVisible(true);
   }

   public void paint( Graphics window )
   {
      smileyFace(window);
   }

   public void smileyFace( Graphics window )
   {
	   int eyeW = 30;
	   int eyeH = 30;
	   int eyeX1 = 310;
	   int eyeX2 = 460;
	   int eyeY = 200;
      window.setColor(Color.BLUE);
      window.drawString("SMILEY FACE LAB ", 35, 35);

      window.setColor(Color.YELLOW);
      window.fillOval( 210, 100, 400, 400 );
      window.setColor(Color.BLUE);
      window.fillOval(eyeX1, eyeY, eyeW, eyeH);
      window.setColor(Color.BLACK);
      window.drawOval(eyeX1, eyeY, eyeW ,eyeH);
      window.setColor(Color.BLUE);
      window.fillOval(eyeX2, eyeY, eyeW, eyeH);
      window.setColor(Color.BLACK);
      window.drawOval(eyeX2, eyeY, eyeW ,eyeH);
      window.setColor(Color.RED);
      window.fillArc(310, 400, (eyeX2-eyeX1), 50, 40, 70);

		//add more code here


   }
}