//(c) A+ Computer Science
//www.apluscompsci.com
//Name -
//Date -

public class CircleRunner
{
 public static void main( String[] args )
   {
  System.out.printf( "Circle area is :: %.2f\n" , Circle.area( 7.5 ) );
  System.out.printf( "Circle perimeter is :: %.2f\n" , Circle.perimeter( 7.5 ) );
  System.out.printf( "Circle area is :: %.2f\n" , Circle.area( 10 ) );
  System.out.printf( "Circle perimeter is :: %.2f\n" , Circle.perimeter( 10 ) );  
  System.out.printf( "Circle area is :: %2f\n", Circle.area(32.15));
  System.out.printf( "Circle perimeter is :: %.2f\n", Circle.perimeter(28));
 }
}