//(c) A+ Computer Science
//www.apluscompsci.com
//Name - Jacob Samurin

import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import static java.lang.System.*;
import java.lang.Math;

public class PrimesRunner
{
	public static void main( String args[] ) throws IOException
	{
		Scanner file = new Scanner(new File("primes.dat"));
		int count = file.nextInt();
		for (int inc =0; inc < count ; inc ++){
			Prime ian = new Prime(file.nextInt());
			out.println(ian);
		}
	}
}
