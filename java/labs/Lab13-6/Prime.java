//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import static java.lang.System.*;
import java.lang.Math;

public class Prime
{
	private int number;

	//constructor methods go here
	public Prime(){
		// number = 0;
		setPrime(0);
	}

	public Prime(int num){
		setPrime(num);
	}

	public void setPrime(int num)
	{
		number = num;
	}

	//boolean isPrime()   goes here
	public boolean isPrime(){
		for (int inc = 2; inc <= Math.sqrt(number);inc++)
			if (number % number == 0)
				return false;
		return true;
	}
	public String toString()
	{
		if (isPrime())
			return number + " is prime";
		return number + "is not prime";
	}
}
