//(c) A+ Computer Science
//www.apluscompsci.com
//Name -

import java.util.Scanner;
import static java.lang.System.*;

public class AverageRunner
{
	public static void main( String args[] )
	{
		String input1 = "9 10 5 20";
		Average test1 = new Average(input1);
		out.println(test1.toString());
		input1 = "11 22 33 44 55 66 77";
		test1 = new Average(input1);
		out.println(test1.toString());
		input1 = "48 52 29 100 50 29";
		test1 = new Average(input1);
		out.println(test1.toString());
		input1 = "0";
		test1 = new Average(input1);
		out.println(test1.toString());
		input1 = "100 90 95 98 100 97";
		test1 = new Average(input1);
		out.println(test1.toString());
	}
}
