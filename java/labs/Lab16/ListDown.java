//(c) A+ Computer Science
//www.apluscompsci.com
//Name -
//Date -

import java.util.ArrayList;
import java.util.List;

public class ListDown
{
	//go() will return true if all numbers in numArray
	//are in decreasing order [31,12,6,2,1]
	public static boolean go(List<Integer> numArray)
	{
		int pre = numArray.get(0);
		boolean anwser = true;
		for(int inc = 1; inc < numArray.size(); inc++){
			if (numArray.get(inc) < pre)
				anwser = true;
			else
				anwser = false;
		}
		return anwser;
	 }	
	public void addToList(int[] array, ArrayList<Integer> list) {
		for (int i : array)
			list.add(i);
	}
}
