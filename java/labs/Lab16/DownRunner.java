//(c) A+ Computer Science
//www.apluscompsci.com
//Name -
//Date -

// import java.util.List;
import java.util.ArrayList;

public class DownRunner
{
	public static void main( String args[] )
	{											
		ListDown l = new ListDown();
		ArrayList<Integer> current = new ArrayList<Integer>();
		int[] array0 = {-99,1,2,3,4,5,6,7,8,9,10,12345};
		l.addToList(array0,current);
		System.out.println(l.go(current));
		int[] array1 = {10,9,8,7,6,5,4,3,2,1,-99};
		l.addToList(array1,current);
		System.out.println(l.go(current));
		int[] array2 = {10,20,30,40,50,-11818,40,30,20,10};
		l.addToList(array2,current);
		System.out.println(l.go(current));
		int[] array3 = {32767};
		l.addToList(array3,current);
		System.out.println(l.go(current));
		int[] array4 = {255,255};
		l.addToList(array4,current);
		System.out.println(l.go(current));
		int[] array5 = {9,10,-88,100,-555,1000};
		l.addToList(array5,current);
		System.out.println(l.go(current));
		int[] array6 = {10,10,10,11,456};
		l.addToList(array6,current);
		System.out.println(l.go(current));
		int[] array7 = {-111,1,2,3,9,11,20,30};
		l.addToList(array7,current);
		System.out.println(l.go(current));
		int[] array8 = {9,8,7,6,5,4,3,2,0,-2,-989};
		l.addToList(array8,current);
		System.out.println(l.go(current));
		int[] array9 = {12,15,18,21,23,1000};
		l.addToList(array9,current);
		System.out.println(l.go(current));
		int[] array10 = {250,19,18,15,13,11,10,9,6,3,2,1,-455};
		l.addToList(array10,current);
		System.out.println(l.go(current));
		int[] array11 = {9,10,-8,10000,-5000,1000};
		l.addToList(array11,current);
		System.out.println(l.go(current));
	}
}
