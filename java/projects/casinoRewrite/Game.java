public class Game{
	private Devices d = new Devices();
	private int winnings,bet,winningTimes = 0;

	public Game(){
		bet = 10;
	}
	public Game(int in){
		bet = in;
	}
	//Rule 3
	public void r3(int choice){
		int d2 = d.d2();
		if(d2 == choice){
			winnings = 2*bet;
			winningTimes = 1;
		}
		else
			winnings = 0;
	}
	//Rule 4
	public boolean r4(String choice){
		String color = d.d17Color();
		boolean output;
		if(color.equals(choice))
			output = true;
		else{
			output = false;
			winnings = 0;
		}
		return output;
	}
	//Rule 5
	public boolean r5() {
		int d2 = d.d2();
		boolean output;
		if(d2 == 1){
			output = true;
			winnings = (3*bet)+bet;
			winningTimes = 1;
		}else
			output = false;
		return output;
	}
	//Rule 6
	public void r6(int choice){
		int d17 = d.d17();
		if(d17 == choice){
			winnings = (bet*24)+bet;
			winningTimes = 1;
		}else 
			winnings = 0;
	}
	//like it says
	public int getWinnings() {
		return winnings;
	}
	public int getWinningTime() {
		return winningTimes;
	}
}
