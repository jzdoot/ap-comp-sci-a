import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Container;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
public class Main extends JFrame implements ActionListener{
	private Slow s = new Slow();
	private Speed f = new Speed();
	public Main(){
		JLabel label = new JLabel("Pick Fast or Slow.");
		JButton fastButton = new JButton("Fast"),slowButton = new JButton("Slow");
		setSize(650,1000);
		setTitle("Winners Only");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		label.setHorizontalAlignment(JLabel.CENTER);
		// button = new JButton("Next");
		// text = new JTextField();
		Container pane = this.getContentPane();
		GridBagLayout bag = new GridBagLayout();
		pane.setLayout(bag);
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 100;
		c.weighty = 50;
		c.gridwidth = 4;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		bag.setConstraints(label, c);
		pane.add(label);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 50;
		c.weighty = 100;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.ipadx = 100;
		c.ipady = 100;
		bag.setConstraints(fastButton, c);
		pane.add(fastButton);
		c = new GridBagConstraints();
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 50;
		c.weighty = 100;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 1;
		c.ipadx = 100;
		c.ipady = 100;
		bag.setConstraints(slowButton, c);
		pane.add(slowButton);
		fastButton.addActionListener(this);
		slowButton.addActionListener(this);
		setVisible(true);
	}
	public void GUI(ArrayList<Integer> a){
		int times = a.get(0), winning = a.get(1), spent = a.get(2), winningTimes = a.get(3);
		setSize(650,1000);
		setTitle("Winners Only");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JLabel label = new JLabel("Number of Iterations == " + times);
		label.setHorizontalAlignment(JLabel.CENTER);
		// button = new JButton("Next");
		// fastButton = new JButton("Fast");
		// slowButton = new JButton("Slow");
		// text = new JTextField();
		Container pane = this.getContentPane();
		pane.removeAll();
		pane.setLayout(new GridLayout(5,1));
		pane.add(label);
		label = new JLabel("Winnings == " + winning);
		label.setHorizontalAlignment(JLabel.CENTER);
		pane.add(label);
		label = new JLabel("Spent == " + spent);
		label.setHorizontalAlignment(JLabel.CENTER);
		pane.add(label);
		label = new JLabel("Profit == " + (winning - spent));
		label.setHorizontalAlignment(JLabel.CENTER);
		pane.add(label);
		label = new JLabel("Win % == " + (((double)winningTimes * 100.0)/(double)times));
		label.setHorizontalAlignment(JLabel.CENTER);
		pane.add(label);
		setVisible(true);
	}
	public static void main(String[] args){
		Main m = new Main();
	}
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("Fast")){
			setVisible(false);
			ArrayList<Integer> a = f.game();
			GUI(a);
		}
		else if(arg0.getActionCommand().equals("Slow")){
			setVisible(false);
			ArrayList<Integer> a = s.game();
			GUI(a);
		}
	}
}
