import static java.lang.System.*;

import java.util.ArrayList;
import java.util.Scanner;
public class Slow{
	private int bet = 10,winning = 0,spent = 0,times = 0, winningTimes = 0;
	private char cChoice = 'y';
	private Devices d = new Devices();
	private Scanner k = new Scanner(System.in);
	public ArrayList<Integer> game(){
		for(int inc = 0; inc<=10; inc++){
			// out.print("[10-50] ");
			out.print("How much do you bet [10-50] :: ");
			bet = k.nextInt();
			while( bet < 10 || bet>50){
				out.println("STOP! The minimum bet is $10 and the maximum is $50");
				out.print("How much do you bet [10-50] :: ");
				bet = k.nextInt();
			}
			Game g = new Game(bet);
			spent += bet;
			int d6 = d.d6(),choice;
			String sChoice;
			out.println("you rolled a " + d6);
			if(d6 == 2 || d6 == 4 || d6 == 6){
				out.print("Please pick a color for the roulette [Red/Green]:: ");
				sChoice = k.next();
				if(g.r4(sChoice)){
					if(g.r5()){
						winning += g.getWinnings();
						out.println(g.getWinnings());
						winningTimes += g.getWinningTime();
					}else{
						out.print("Please pick a number for the roulette [1-17] ");
						choice = k.nextInt();
						g.r6(choice);
						winning += g.getWinnings();
						out.println(g.getWinnings());
						winningTimes += g.getWinningTime();
					}
				}else{
					winning += g.getWinnings();
					out.println(g.getWinnings());
						winningTimes += g.getWinningTime();
				}
			}else if(d6 == 3 || d6 == 5){
				out.print("Please pick a side of a coin [1/2]");
				choice = k.nextInt();
				g.r3(choice);
				winning += g.getWinnings();
				out.println(g.getWinnings());
						winningTimes += g.getWinningTime();
			}else{
				winning += 0;
				out.println(0);
			}
			times++;
		}
		out.println("Numbers of Iterations == " + times);
		out.println("Winnings == " + winning);
		out.println("Spent == " + spent);
		out.println("Profit == " + (winning - spent));
		ArrayList<Integer> re = new ArrayList<Integer>();
		re.add(times);
		re.add(winning);
		re.add(spent);
		re.add(winningTimes);
		return re;
	}
}
