//Jacob Samurin
import java.util.Scanner;
import static java.lang.System.*;
public class Main {
	public static void main(String[] args) {
		String make, model, plate1, plateFinal;
		int plate2, plate10 = 0, plateNew;
		String[] alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
		Scanner key = new Scanner(System.in);
		out.print("Input String\nMake :: ");
		make = key.next();
		out.print("Model :: ");
		model = key.next();
		out.print("Plate :: ");
		plate1 = key.next();
		plate2 = key.nextInt();
		key.close();
		plate10 += (int)plate1.charAt(0);
		plate10 += (int)plate1.charAt(1);
		plate10 += (int)plate1.charAt(2);
		plateNew = plate10 + plate2;
		plateNew = plateNew % 26;
		plateFinal = alphabet[plateNew] + (plate10 + plate2);
		out.println("Make == " + make + "\nModel == " + model + "\n" + plate1 + " " + plate2 + " == " + plateFinal + "\nRJK 492 == V723" + "\nSPT 109 == K556");
	}
}
