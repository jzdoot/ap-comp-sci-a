import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

class Main {
  public static void main(String[] args) throws IOException {
    Scanner file = new Scanner(new File ("dictionary.txt"));
    ArrayList<String> wordList = new ArrayList<String>();
    String str;
    while (file.hasNext()) {
      str = file.next();
      wordList.add(str);
    }
    String[] words = wordList.toArray(new String[0]);
  

    System.out.println("\nLevel 1: problem 1 (2 points)");
    System.out.println(words.length);
    
    System.out.println("\nLevel 1: problem 2 (2 points)");
    int max = words[0].length();
    for (int i = 1; i < words.length; i++) {
      if (words[i].length()>max)
        max = words[i].length();
    }
    for (int i = 0; i < words.length; i++) {
      if (words[i].length()==max)
        System.out.println(words[i]);
    }

    System.out.println("\nLevel 1: problem 3 (2 points)");
    System.out.println(" Words with no vowels:");
    for (int i = 0; i < words.length; i++) {
      if(!words[i].contains("a") && !words[i].contains("e") && !words[i].contains("i") && !words[i].contains("o") && !words[i].contains("u"))
        System.out.println(words[i]);
    }
    System.out.println(" Words with no vowels AND no y:");
    for (int i = 0; i < words.length; i++) {
      if(!words[i].contains("a") && !words[i].contains("e") && !words[i].contains("i") && !words[i].contains("o") && !words[i].contains("u") && !words[i].contains("y"))
        System.out.println(words[i]);
    }

    System.out.println("\nLevel 1: problem 4 (2 points)");
    double total = 0.0;
    for (String item : words) {
      total += item.length();
    }
    total = total/(words.length);
    total = (double) Math.round(total*100)/100;
    System.out.println(total);

    System.out.println("\nLevel 1: problem 5 (2 points)");
    int count = 0;
    for (String item : words) {
      if (item.charAt(0)==(item.charAt(item.length()-1)))
        count++;
    }
    System.out.println(count);

    
    
    
    System.out.println("\nLevel 2: problem 1 (4 points)");

    
    int[] aValues = new int[26];
    
     for(String item : words)
      for(int x = 0; x < item.length(); x++)
        if(item.charAt(x) >= 97 && item.charAt(x) <= 122)
          aValues[item.charAt(x) - 97]++;

    System.out.println(Arrays.toString(aValues));
    int maxPos = 0;
    int maxx = aValues[maxPos];
    for (int x = 1; x < aValues.length; x++)
      if (aValues[x] > maxx)
        maxPos = x;
    int number = maxPos + 97;
    char i = (char)number ;

    System.out.println("Most common letter is " +i);
    


    
    System.out.println("\nLevel 2: problem 2 (4 points)");

    int[] length = new int[40];
    int countt = 0;
    for(String item : words)
      {
        for(int x = 0; x < item.length(); x++)
        {
          countt++;
        }
        length[countt]++;
        countt = 0;
      }

    System.out.println(Arrays.toString(length));
    int mostCommon = 0;
    int numMostCommon = length[mostCommon];
    for (int x = 1; x < length.length; x++)
      {
        if (length[x] > length[mostCommon])
          mostCommon = x;
          numMostCommon = length[mostCommon];
      }

    System.out.println("The most common length is " +mostCommon);
    System.out.println("There are " +numMostCommon+ " with the length " + mostCommon);
    


    
    System.out.println("\nLevel 2: problem 4 (4 points)");
    for (String item : words) {
      char[] forward = new char[item.length()];
      for (int l = 0; l < item.length(); l++){
        forward[l] = item.charAt(l);
      }
    
      char[] backward = new char[item.length()];
      
      int len = item.length();
      for (int a = 0; a < item.length(); a++){
        backward[len - 1] = forward[a];
        len = len - 1;
      }
      
    if (Arrays.equals(forward,backward)){
      System.out.println(item);
    }
  }

    
    System.out.println("\nLevel 2: problem 5 (4 points)");

    int[] alphabet = new int[26];
    
     for(String item : words)
      for(int x = 0; x < item.length()-1; x++)
        if(item.charAt(x) >= 97 && item.charAt(x) <= 122 && item.charAt(x+1) == item.charAt(x))
          alphabet[item.charAt(x) - 97]++;

    System.out.println(Arrays.toString(alphabet));
    int mP = 0;
    for (int x = 1; x < alphabet.length; x++)
      if (alphabet[x] > alphabet[mP])
        mP = x;
    int num = maxPos + 97;
    char x = (char)number ;

    System.out.println("Most common double letter combination is "+i);
    

    

    System.out.println("\nLevel 3: problem 3 (6 points)");
    
    ArrayList<String> dollarList = new ArrayList<String>();
    int wordPrice = 0;
    int value = 0;
    
    for (String item : words) {
      for (int d = 0; d < item.length(); d++) {
        value = ((int)item.charAt(d)) - 96;
        wordPrice += value;
      }
      if (wordPrice == 100) {
        System.out.print("" + item + " ");
        dollarList.add(item);
      }
      wordPrice = 0;
    }
    
    String shortest = dollarList.get(0);
    for (int t = 0; t < dollarList.size(); t++) {
      if (dollarList.get(t).length() < shortest.length())
        shortest = dollarList.get(t);
    }
    System.out.println("Shortest dollar word is: " + shortest);
  }
}