import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

class Main {
  public static void main(String[] args) throws FileNotFoundException{
	  //adding all the words from the dictionary.txt file to an ArrayList
	ArrayList<String> list = new ArrayList<String>();
	Scanner file = new Scanner(new File("dictionary.txt"));
	while(file.hasNext()){
		list.add(file.next());
	}
	//Level 1
	//System.out.println("No Vowels");
	//for(String item:new Main().noVowels(list))
	//	System.out.println(item);
	////level 2
	//System.out.println("");
	//new Main().commonLetters(list);
	////level 3
	//System.out.println("");
	//for(String item: new Main().vowels(list))
	//	System.out.println(item);
	//System.out.println("");
	//for(Sting item: new Main().dollarWords(list))
	//	System.out.println(item);
	//System.out.println("");
	//for(String item: new Main().fourUsing2(list))
	//	System.out.println(item);
  }
  public int startEnd(ArrayList<String> list){
	  //2
	  int count =0;
	  for(String item: list)
		  if (item.charAt(0) == item.charAt(item.length()-1))
			  count++;
	  return count;
  }
  public double averageLength(ArrayList<String> list){
	  //2
	  double total=0;
	  for(String item:list)
		  total += item.length();
	  return total/list.size();
  }
  public String longestWord(ArrayList<String> list){
	  //2
	  String longest = "";
	  for(String item:list)
		  if(longest.length()<item.length())
			  longest = item;
	  return longest;
  }
  public int countWords(ArrayList<String> list){
	  //2
	  return list.size();
  }
  public ArrayList<String> noVowels(ArrayList<String> list){
	  //2
	  ArrayList<String> temp = new ArrayList<String>();
	  for(String item:list){
		  if(!(item.contains("a") || item.contains("e") || item.contains("i") || item.contains("o") || item.contains("u")))
			  temp.add(item);
	  }
	  System.out.println("There are " + temp.size() + " word that have no vowels");
	  // for(String item:temp)
		  // System.out.println(item+",");
	  return temp;
  }
  private String[][] commonLettersSort(String[][] alphabet, ArrayList<String> list){
	  //4
	  //got from Lab24-8 APIBSORT and modifyed to work
	 for(int last = alphabet[1].length-1;last>0;last--){
		 for(int current=0;current<last;current++){
			 if(alphabet[1][current].compareTo(alphabet[1][current+1])>0){
				 String temp0 = alphabet[0][current];
				 String temp1 = alphabet[1][current];
				 alphabet[0][current]=alphabet[0][current+1];
				 alphabet[1][current]=alphabet[1][current+1];
				 alphabet[0][current+1]=temp0;
				 alphabet[1][current+1]=temp1;
			 }
		 }
	 // out.println("Pass "+pass+Arrays.toString(list));
	 }
	 int total = 0;
	 for(String item:list)
		 for(int i = 0; i < item.length();i++)
			 total++;
	 for(int i = 0 ; i < alphabet[1].length;i++){
		 double number = Double.parseDouble(alphabet[1][i]);
		 alphabet[1][i] = ((number / (double)total) * 100 ) + "";
	 }
	  return alphabet;
  }
  public String[][] commonLetters(ArrayList<String> list){
	  String[][] alphabet = 
	  {
		  {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"},
		  {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"}
	  };
	  for(String item: list){
		  for(int i = 0; i<item.length();i++){
			  switch(item.charAt(i)){
				  case 'a': alphabet[1][0] = Integer.parseInt(alphabet[1][0]) + 1 + "";
							break;
				  case 'b': alphabet[1][1] = Integer.parseInt(alphabet[1][1]) + 1 + "";
							break;
				  case 'c': alphabet[1][2] = Integer.parseInt(alphabet[1][2]) + 1 + "";
							break;
				  case 'd': alphabet[1][3] = Integer.parseInt(alphabet[1][3]) + 1 + "";
							break;
				  case 'e': alphabet[1][4] = Integer.parseInt(alphabet[1][4]) + 1 +"";
							break;
				  case 'f': alphabet[1][5] = Integer.parseInt(alphabet[1][5]) + 1 + "";
							break;
				  case 'g': alphabet[1][6] = Integer.parseInt(alphabet[1][6]) + 1 + "";
							break;
				  case 'h': alphabet[1][7] = Integer.parseInt(alphabet[1][7]) + 1 + "";
							break;
				  case 'i': alphabet[1][8] = Integer.parseInt(alphabet[1][8]) + 1 + "";
							break;
				  case 'j': alphabet[1][9] = Integer.parseInt(alphabet[1][9]) + 1 + "";
							break;
				  case 'k': alphabet[1][10] = Integer.parseInt(alphabet[1][10]) + 1 + "";
							break;
				  case 'l': alphabet[1][11] = Integer.parseInt(alphabet[1][11]) + 1 + "";
							break;
				  case 'm': alphabet[1][12] = Integer.parseInt(alphabet[1][12]) + 1 + "";
							break;
				  case 'n': alphabet[1][13] = Integer.parseInt(alphabet[1][13]) + 1 + "";
							break;
				  case 'o': alphabet[1][14] = Integer.parseInt(alphabet[1][14]) + 1 + "";
							break;
				  case 'p': alphabet[1][15] = Integer.parseInt(alphabet[1][15]) + 1 + "";
							break;
				  case 'q': alphabet[1][16] = Integer.parseInt(alphabet[1][16]) + 1 + "";
							break;
				  case 'r': alphabet[1][17] = Integer.parseInt(alphabet[1][17]) + 1 + "";
							break;
				  case 's': alphabet[1][18] = Integer.parseInt(alphabet[1][18]) + 1 + "";
							break;
				  case 't': alphabet[1][19] = Integer.parseInt(alphabet[1][19]) + 1 + "";
							break;
				  case 'u': alphabet[1][20] = Integer.parseInt(alphabet[1][20]) + 1 + "";
							break;
				  case 'v': alphabet[1][21] = Integer.parseInt(alphabet[1][21]) + 1 + "";
							break;
				  case 'w': alphabet[1][22] = Integer.parseInt(alphabet[1][22]) + 1 + "";
							break;
				  case 'x': alphabet[1][23] = Integer.parseInt(alphabet[1][23]) + 1 + "";
							break;
				  case 'y': alphabet[1][24] = Integer.parseInt(alphabet[1][24]) + 1 + "";
							break;
				  case 'z': alphabet[1][25] = Integer.parseInt(alphabet[1][25]) + 1 + "";
							break;
				  default: System.out.println("Error:LetterNotFound");
						   break;
			  }
		  }
	  }
	  for(String[] arr: commonLettersSort(alphabet, list)){
		  for(String item: arr){
			  System.out.print(item + ", ");
		  }
		  System.out.println();
	  }
	  return commonLettersSort(alphabet,list);
  }
  public ArrayList<String> dollarWords(ArrayList<String> list){
	  //6
	  ArrayList<String> temp = new ArrayList<String>();
	  for(String item: list){
		  int total = 0;
		  for(int i = 0 ; i < item.length(); i++){
			  total += item.charAt(i)-96;
		  }
		  if(total == 100)
			  temp.add(item);
	  }
	  return temp;
  }
  public String vowels(ArrayList<String> list){
	  //TODO
	  //6
	  return "";
  }
}
