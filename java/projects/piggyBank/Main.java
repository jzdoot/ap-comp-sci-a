//Jacob Samurin
import java.util.Scanner;
import static java.lang.System.*;
public class Main {
	public static void main(String[] args) {
		out.println("0 == no\n1 == yes");
		int answer, penny, nickel, dime, quarter;
		Scanner keyboard = new Scanner(System.in);
		out.print("Do you want to set an initial value for your piggy bank [0/1] :: ");
		answer = keyboard.nextInt();
		if (answer == 1) {
			out.print("Number of Pennys :: ");
			penny = keyboard.nextInt();
			out.print("Number of Nickels :: ");
			nickel = keyboard.nextInt();
			out.print("Number of Dimes :: ");
			dime = keyboard.nextInt();
			out.print("Number of Quarters :: ");
			quarter = keyboard.nextInt();
			out.println("Curent Number of Pennys :: " + penny + "\nCurrent Number of Nickels :: " + nickel + "\nCurrent Number of Dimes :: " + dime + "\nCurrent Number of Quarters :: " + quarter + "\nTotal Amount :: $" + ((((double)penny) + (((double)nickel) * 5) + (((double)dime) * 10) + (((double)quarter) * 25))/100));
		}else {
			penny = 0;
			nickel = 0;
			dime = 0;
			quarter = 0;
			out.println("Curent Number of Pennys :: " + penny + "\nCurrent Number of Nickels :: " + nickel + "\nCurrent Number of Dimes :: " + dime + "\nCurrent Number of Quarters :: " + quarter + "\nTotal Amount :: $" + ((((double)penny) + (((double)nickel) * 5) + (((double)dime) * 10) + (((double)quarter) * 25))/100));
		}
			piggyBank bank = new piggyBank(penny, nickel, dime, quarter);
		out.print("What do you want to do?\n0 == View Curent Balance\n1 == Add\n2 == Subtract\n:: ");
		answer = keyboard.nextInt();
		if (answer == 0) {
			bank.print();
		}else if (answer == 1) {
			out.print("Number of Pennys :: ");
			penny = keyboard.nextInt();
			out.print("Number of Nickels :: ");
			nickel = keyboard.nextInt();
			out.print("Number of Dimes :: ");
			dime = keyboard.nextInt();
			out.print("Number of Quarters :: ");
			quarter = keyboard.nextInt();
			bank.add(penny, nickel, dime, quarter);
		}else {
			out.print("Number of Pennys :: ");
			penny = keyboard.nextInt();
			out.print("Number of Nickels :: ");
			nickel = keyboard.nextInt();
			out.print("Number of Dimes :: ");
			dime = keyboard.nextInt();
			out.print("Number of Quarters :: ");
			quarter = keyboard.nextInt();
			bank.sub(penny, nickel, dime, quarter);
		}
		keyboard.close();
	}

}
