/*
   the best example of this is in Named Cow, it is iheriting stuff from the parent class Cow, and cow is getting methods and stiff from the Animal interface. As well as all of that the Pig and Chick class also inhariate the getSound() and GetType() methods from the animal interface.
*/
public class Farm
{
//instance variable should be an array of 3 Animals //constructor should add a Cow, Chicken and Pig object to array
    private Animal[] pen= new Animal[3];
public Farm(Pig pig, Chick chick, NamedCow cow){
    pen[0]=pig;
    pen[1]=chick;
    pen[2]=cow;
}
   public void animalSounds()    {
// loop through all animals in array and print type and sound
// Sample output for animal: Cow goes moo }
       for(int i=0;i<pen.length-1;i++){
           Animal other = (Animal)pen[i];
           System.out.println(other.getType() + " says " + other.getSound());
           }
		NamedCow other = (NamedCow)pen[2];
		System.out.println(other.getType() + " named " + other.getName() + " says " + other.getSound());
   }
}
