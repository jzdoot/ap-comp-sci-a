public interface Animal {
//note that all methods of an interface are automatically public: 
	String getSound();
	String getType();
}
