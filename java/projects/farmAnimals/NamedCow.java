public class NamedCow implements Animal{
	private String myType="", mySound="", myName="";
	public NamedCow(String type, String sound, String name){
		myType = type;
		mySound = sound;
		myName = name;
	}
	public String getSound(){
		return mySound;
	}
	public String getType(){
		return myType;
	}
	public String getName(){
		return myName;
	}
}
