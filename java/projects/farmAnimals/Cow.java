public class Cow implements Animal
{
private String myType; 
private String mySound;

public Cow(String type, String sound)
     {
       myType = type;
       mySound = sound;
     }
//note that the following two methods now HAVE to be implemented: 
public String getSound() { return mySound; }

public String getType() 
{ 
  return myType; 
} 
}