import java.util.Scanner;

public class Main{
	public static void main(String[] args){
		Scanner k = new Scanner(System.in);
		String choice = "";
		char cChoice = 'y';
		do{
			for(int i=0; i<=1;i++){
				System.out.print("[Fast/Slow/No] ");
				choice = k.next();
				if(choice.equals("Slow"))
					Slow.main(args);
				else if(choice.equals("Fast"))
					Speed.main(args);
				
			}
			System.out.print("Continue [y/n] :: ");
			String sChoice = k.next();
			if(sChoice.charAt(0) != cChoice)
				cChoice = 'n';
		} while (cChoice == 'y');
		k.close();
	}
}
