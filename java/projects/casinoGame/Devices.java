public class Devices{
	//make a d6 method

	public int d6(){
		int num = (int)(Math.random()*(6-1+1)+1);
		return num;
	}
	//make a d2/coin method
	public int d2() {
		int num = (int)(Math.random()*(2-1+1)+1);
		return num;
	}
	//make a d17/roulette method
	public int d17() {
		int num = (int)(Math.random()*(17-1+1)+1);
		return num;
	}
	
	public String d17Color() {
		int testNum = d17();
		String color;
		if(testNum == 17)
			color = "Black";
		else if(testNum <= 8)
			color = "Green";
		else
			color = "Red";
		return color;
	}
}
