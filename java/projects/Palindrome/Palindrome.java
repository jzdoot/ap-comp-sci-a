// inport java.util.Scanner;
import java.util.ArrayList;
public class Palindrome{
	private String palindrome;

	public Palindrome(){
		set("racecar");
	}
	public Palindrome(String pal){
		set(pal);
	}
	public void set(String pal) {
		palindrome = pal;
	}
	public String chop(String pal) {
		String changed = pal;
		ArrayList<Character> temp = new ArrayList<Character>();
		String finished = "";
		for(int inc = 0; inc < changed.length();inc++)
			temp.add(changed.charAt(inc));
		for(int dec = temp.size()-1; dec >= 0; dec--){
			//TODO add all chars
			if(temp.get(dec) == ' ' || temp.get(dec) == ',' || temp.get(dec) == '&' || temp.get(dec) == '.' || temp.get(dec) == '?')
				temp.remove(dec);
		}
		for(int inc = 0 ; inc < temp.size(); inc++)
			finished += temp.get(inc);
		return finished.toLowerCase();
	}
	public String flip(String pal) {
		String changed = pal;
		ArrayList<Character> temp = new ArrayList<Character>();
		String finished = "";
		for(int inc = 0; inc < changed.length();inc++)
			temp.add(changed.charAt(inc));
		for(int dec = temp.size()-1;dec >= 0; dec--)
			finished += temp.get(dec);
		return finished;
	}
	public boolean get() {
		String pal = chop(palindrome);
		boolean out;
		if(pal.equals(flip(pal)))
			out = true;
		else
			out = false;
		return out;
	}
}
