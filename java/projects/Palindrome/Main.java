// Name - Jacob Samurin
// Project - Palendromes
import java.util.Scanner;
import static java.lang.System.*;
class Main {
	public static void main(String[] args) {
		Scanner k = new Scanner(System.in);
		char response;
		out.println("Enter a Palindrome [String] :: ");
		String in = k.nextLine();
		Palindrome pal = new Palindrome(in);
		if(pal.get())
			out.println("\"" + in + "\" is a palindrome");
		else
			out.println("\"" + in + "\" is not a palindrome");
		out.print("Do you want to contune [y/n] :: ");
		String str;
		str = k.next();
		response=str.charAt(0);
		while (response == 'y' || response == 'Y'){
			out.println("Enter a Palindrome [String] :: ");
			k.nextLine();
			in = k.nextLine();
			pal.set(in);
			if(pal.get())
				out.println("\"" + in + "\" is a palindrome");
			else
				out.println("\"" + in + "\" is not a palindrome");
			out.print("Do you want to contune [y/n] :: ");
			str = k.next();
			response=str.charAt(0);
		}
	}
}
