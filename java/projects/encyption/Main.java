import java.util.Scanner;

class Main {
	public static String encrypt(String word, int key){
		char[] arrWord = new char[word.length()];
		int[] intWord = new int[word.length()];
		String out = "";
		for(int i=0;i<word.length();i++)
			arrWord[i] = word.charAt(i);
		for(int i=0;i<arrWord.length;i++)
			intWord[i] = arrWord[i] + key;
		for(int i=0;i<arrWord.length;i++)
			out = out+ intWord[i]+" ";
		return out;
	}
	public static String decrypt(String word,int key){
		char[] arrWord = new char[word.length()];
		int[] intWord = new int[word.length()];
		String out = "";
		for(int i=0;i<word.length();i++)
			arrWord[i] = word.charAt(i);
		for(int i=0;i<arrWord.length;i++)
			intWord[i] = arrWord[i] - key;
		for(int i=0;i<arrWord.length;i++)
			out = out+ intWord[i]+" ";
		return out;
	}
  public static void main(String[] args) {
	  Scanner k = new Scanner(System.in);
	  System.out.print("Encrypt/Decrypt[0/1]:: ");
	  boolean choice0=(k.nextInt()!=0);
	  if(choice0){
		  System.out.print("Key[int]:: ");
		  int choice1 = k.nextInt();
		  System.out.print("Word[String]:: ");
		  String choice2 = k.next();
		  System.out.println(encrypt(choice2, choice1));
	  }else{
		  System.out.print("Key[int]:: ");
		  int choice1 = k.nextInt();
		  System.out.print("Word[String]:: ");
		  String choice2 = k.next();
		  System.out.println(decrypt(choice2, choice1));
	  }
	  k.close();
  }
}
