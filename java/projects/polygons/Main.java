import java.util.Scanner;

class Main {
  public static void main(String[] args) {
	  Scanner k = new Scanner(System.in);
	  int count,x,y;
	  IrregularPolygon shape = new IrregularPolygon();
	  System.out.print("How many points?[int] ");
	  count = k.nextInt();
	  while(count > 0){
		  System.out.print("Enter x:[int] ");
		  x = k.nextInt();
		  System.out.print("Enter y:[int] ");
		  y = k.nextInt();
		  Point p = new Point(x,y);
		  shape.add(p);
		  count--;
	  }
	  System.out.println("Perimeter:: " + shape.perimeter() + "\nArea:: " + shape.area());
	  k.close();
  }
}
