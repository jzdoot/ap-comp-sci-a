public class Point{
	private double[] xy = new double[2];
	
	public Point(){
		xy[0] = 0;
		xy[1] = 0;
	}
	public Point(int x, int y){
		xy[0] = x;
		xy[1] = y;
	}
	public void setX(int x){
		xy[0] = x;
	}
	public void setY(int y){
		xy[1] = y;
	}
	public double getX(){
		return xy[0];
	}
	public double getY(){
		return xy[1];
	}
	public double distance(Object obj){
		Point other = (Point)obj;
		return Math.sqrt(Math.pow((other.xy[0]-this.xy[0]), 2)+Math.pow((other.xy[1]-this.xy[1]), 2));
	}
	public boolean equals(Object obj){
		Point other = (Point)obj;
		return (other.xy[0] == this.xy[0] && other.xy[1] == this.xy[1]);
	}
}
