import java.util.ArrayList;

public class IrregularPolygon{
	private ArrayList<Point> list;
	public IrregularPolygon(){
		list = new ArrayList<Point>();
	}
	public void add(Point p){
		list.add(p);
	}
	public double perimeter(){
		double perimeter = 0;
			for (int i = 1; i < list.size(); i++) {
			perimeter += list.get(i-1).distance(list.get(i));
		}
		perimeter += list.get(list.size()-1).distance(list.get(0));
		return perimeter;
	}
	public double area(){
		double area = 0;
		for (int i = 1; i < list.size(); i++) {
			area += (list.get(i-1).getX()*list.get(i).getY());
		}
		area += (list.get(list.size()-1).getX()*list.get(0).getY());
		for (int i = 1; i < list.size(); i++) {
			area -= (list.get(i-1).getY()*list.get(i).getX());
		}
		area -= (list.get(list.size()-1).getY()*list.get(0).getX());
		return Math.abs(.5*area);
	}
}
