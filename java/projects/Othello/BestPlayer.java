import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
public class BestPlayer extends Player{
	public BestPlayer(Color c, String n){
		super(c,n);
	}
	//check for all legal moves
	private ArrayList<Point> allLegalMoves(Board theBoard){
		ArrayList<Point> temp = new ArrayList<Point>();
		for(int row = 0; row<theBoard.getRows(); row++){
			for(int col = 0; col<theBoard.getColumns();col++){
				if(theBoard.isLegal(row, col, super.myColor))
					temp.add(new Point(row,col));
			}
		}
		return temp;
	}
	public Point getMove(Board theBoard){
		Board og = theBoard.getCopy();
		ArrayList<Point> list = allLegalMoves(theBoard);
		int[] moves = new int[list.size()];
		//TODO make a loop for all legal moves
		for(int i =0 ; i<list.size();i++){
			Board updated = theBoard.getCopy();
			updated.placePiece((int)list.get(i).getY(), (int)list.get(i).getX(), super.myColor);
			//TODO make the nested loops to check amount of difrences
			int count = 0;
			for(int row = 0; row<theBoard.getRows(); row++){
				for(int col = 0; col<theBoard.getColumns();col++){
					if(!og.getState(row, col).equals(updated.getState(row, col)))
							count++;
				}
			}
		}
		return output;
	}
}
