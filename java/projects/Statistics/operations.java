import java.util.Arrays;

class operations{
	public double findMean(int[] array) {
		int sum = 0;
		double mean = 0;
		for(int item : array)
			sum += item;
		mean = (double)sum/array.length;
		return mean;
	}
	public double findStandardDeviation(int[] array) {
		double step2 = 0, deviation, mean = findMean(array);
		for(int item : array){
			double step1 = Math.pow((item - mean), 2);
			step2 += step1;
		}
		deviation = Math.sqrt(step2/(array.length - 1));
		return deviation;
	}
	public int findRange(int[] array) {
		int range;
		int[] tempArray = array;
		Arrays.sort(tempArray);
		range = tempArray[tempArray.length-1] - tempArray[0];
		return range;
	}
	public int findMode(int[] array) {
		int[] appears = new int[101];
		int mode = 0;
		for(int i = 0; i < appears.length; i++){
			int k = array[i];
			appears[k]++;
		}
		int max = 0;
		for(int i = 0; i < appears.length; i++){
			if(appears[i] > max)
				max=appears[i];
		}
		for(int i : appears){
			if(i == max)
				mode = i;
		}
		return mode;
	}
}
