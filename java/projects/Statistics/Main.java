import static java.lang.System.*;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;

// DO NO USE ARRAYSLIST
class Main{
	public static void main(String[] args) throws IOException {
		//Vars
		int count = 0, range, mode;
		double mean, deviation;
		Scanner file = new Scanner(new File("numbers.txt"));
		operations op = new operations();
		while (file.hasNextInt()) {
			count++;
			file.nextInt();
		}
		int[] number = new int[count];
		out.println(count);
		file.close();
		file = new Scanner(new File("numbers.txt"));
		for(int inc = 0 ; inc < count; inc++)
			number[inc] = file.nextInt();
		file.close();
		//mean
		mean = op.findMean(number);
		out.printf("The Mean is %.2f%n", mean);
		//Standard Deviation
		deviation = op.findStandardDeviation(number);
		out.printf("The Standard Deviation %.2f%n",deviation);
		//Range
		range = op.findRange(number);
		out.println("The range " + range);
		//mode
		mode = op.findMode(number);
		out.println("The mode is " + mode);
	}
}
